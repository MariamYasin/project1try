package test;

import java.io.IOException;

import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class Test_Post {

	@DataProvider(name = "data")
	public Object[][] dataSource() throws IOException {

		Object[][] dat = read1.readCSVdata();
		return dat;

	}

	@Test(dataProvider = "data")
	public void post(String ecrust, String eflavor, String eorder_id, String esize, String etable_no,
			String etimetamp) {
		RequestSpecification httprequest = RestAssured.given();
		httprequest.header("Content-Type", "application/json");
		JSONObject json = new JSONObject();
		json.put("ecrust", ecrust);
		json.put("eflavor", eflavor);
		// json.put("eorder_id",eorder_id);
		json.put("esize", esize);
		json.put("etable_no", etable_no);
		// json.put("etimetamp",etimetamp);
		httprequest.body(json.toJSONString());
		Response response = httprequest.post("https://order-pizza-api.herokuapp.com/api/orders");

		int statusCode = response.getStatusCode();
		String responseBody = response.asString();
		// System.out.println(responseBody);
		Assert.assertEquals(responseBody.contains("Missing Authorization Header"), true);
		// Assert.assertEquals(responseBody.contains(eflavor),true);
//				//Assert.assertEquals(responseBody.contains(eorder_id),true);
//				Assert.assertEquals(responseBody.contains(esize),true);
//				Assert.assertEquals(responseBody.contains(etable_no),true);
//				//Assert.assertEquals(responseBody.contains( etimetamp),true);
		Assert.assertEquals(statusCode, 401);

		// System.out.println(statusCode);
	}

	@Test(dataProvider = "data")
	public void getOrder(String ecrust, String eflavor, String eorder_id, String esize, String etable_no,
			String etimetamp) {
		String data = RestAssured.get("https://order-pizza-api.herokuapp.com/api/orders").asString();
		int code = RestAssured.get("https://order-pizza-api.herokuapp.com/api/orders").getStatusCode();
		Assert.assertEquals(code, 200);

	}

	@Test(dataProvider = "data")
	public void delete(String ecrust, String eflavor, String eorder_id, String esize, String etable_no,
			String etimetamp) {
		RequestSpecification request = RestAssured.given();
		Response response = request.delete("https://order-pizza-api.herokuapp.com/api/orders/"+eorder_id);
		// System.out.println("https://order-pizza-api.herokuapp.com/api/orders/"+eorder_id);
		int code = response.getStatusCode();
		// System.out.println(code);
		Assert.assertEquals(code, 404);// not allowed for delete request
	}
}
